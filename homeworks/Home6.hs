-- арифметические прогрессии длины 3 с расстоянием k
-- например, take 2 (arith 2) = [[5,11,17], [7,13,19]]
-- Натуральные числа (начинаем с 1, хотя можно поспорить)
-- nat = [1..]
import Control.Monad

nat = 1 : map (+1) nat

-- Проверка на простоту (почти в лоб)
isPrime 1 = False
isPrime 2 = True
isPrime n | even n = False
          | otherwise = not $ any (\i -> n`mod`i == 0) $ takeWhile (\i -> i*i<=n) [3,5..]

-- Простые числа -- подмножество натуральных такое, что каждый элемент прост
primes = filter isPrime nat

isArith :: (Ord a, Num a) => (a, (a, a)) -> Bool
isArith (el1,(el2,el3)) = el2 - el1 == el3 - el2

iterateWithFiltering :: (Ord b, Num b) => [(b, (b, b))] -> [[b]]
iterateWithFiltering [] = [] 
iterateWithFiltering (x:xs) | isArith x = [fst x, fst $ snd x, snd $ snd x]:iterateWithFiltering xs
							| otherwise = iterateWithFiltering xs

tailN 0 list = list
tailN n list = tailN (n-1) (tail list) 

arith k = iterateWithFiltering ar where 
	secondList = tailN k primes 
	ar = zip primes $ zip secondList (tailN k secondList)

-- [(2,a), (4, b), (6, c), (8, d), (3, e), (1, f), (7, g), (5, h)]
-- положение ферзей на шахматной доске
-- список номеров горизонталей, на которых находятся ферзи
-- например, Board [1,2,3,4,5,6,8,7] -- это такое расположение
--  +--------+
-- 8|      ♕ |
-- 7|       ♕|
-- 6|     ♕  |
-- 5|    ♕   |
-- 4|   ♕    |
-- 3|  ♕     |
-- 2| ♕      |
-- 1|♕       |
-- -+--------+
--  |abcdefgh|
newtype Board = Board { unBoard :: [Int] } deriving (Eq,Show)
 
emptyBoard = Board []
queens n = filter test (generate n) where	
	generate :: Int -> [Board] 
	generate 0 = [Board []]
	generate k = [ Board (q:(unBoard qs)) | q <- [1..n], qs <- generate (k-1)]
	test (Board []) = True
	test (Board (x:xs)) = isSafe x xs && test (Board xs)
	isSafe q qs = not (q `elem` qs || sameDiag q qs)
	sameDiag q qs = any (\(colDist, p) -> abs(q - p) == colDist) $ zip [1..] qs	
{-
queens :: Int -> [Board]
queens n = undefined

-- Белые начинают и дают мат в два хода
-- 
-- Белые: Фf8 g4 Крh2
-- Черные: g5 h5 a4 Крh4
-- 
-- (написать перебор всех вариантов полуходов длины три,
-- вернуть список последовательностей полуходов, ведущих
-- к решению; до этого определить необходимые типы)
-- См. https://en.wikipedia.org/wiki/Chess_notation

solutions :: [[Move]]

-} 