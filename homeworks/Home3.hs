
-- | tribo_n = tribo_{n-1} + tribo_{n-2} + tribo_{n-3}
--   tribo_0 = 1
--   tribo_1 = 1
--   tribo_2 = 1
--
-- >>> tribo 0
-- 1
--
-- >>> tribo 1
-- 1
--
-- >>> tribo 2
-- 1
--
-- >>> tribo 3
-- 3
--
-- >>> odd (tribo 100)
-- True
tribo :: Int -> Int
tribo n = tribo1 n 1 1 1 where
	tribo1 0 a b c = a
	tribo1 1 a b c = b
	tribo1 2 a b c = c
	tribo1 n a b c = tribo1 (n-1) b c (a + b + c)



-- Тип Цвет, который может быть Белым, Черным, Красным, Зелёным, Синим, либо Смесь из трёх чисел (0-255)
-- операции Сложение :: Цвет -> Цвет -> Цвет
-- (просто складываются интенсивности, если получилось >255, то ставится 255)
-- ПолучитьКрасныйКанал, ПолучитьЗелёныйКанал, ПолучитьСинийКанал :: Цвет -> Int

data Color = ColorConstructor {red::Int, green::Int, blue::Int}
			| White 
			| Black
			| Red
			| Green
			| Blue
			| Purple
			deriving(Show)

getColor :: Color -> Color

getColor c@(ColorConstructor{}) = c
getColor w@White = ColorConstructor 0 0 0
getColor b@Black = ColorConstructor 255 255 255
getColor r@Red = ColorConstructor 255 0 0
getColor g@Green = ColorConstructor 0 255 0 
getColor b@Blue = ColorConstructor 0 0 255
getColor p@Purple = ColorConstructor 139 0 255

getRedChannel :: Color -> Int
getRedChannel c@ColorConstructor{} = red c
getRedChannel c = getRedChannel $ getColor c

getGreenChannel :: Color -> Int
getGreenChannel c@ColorConstructor{} = green c
getGreenChannel c = getGreenChannel $ getColor c

getBlueChannel :: Color -> Int
getBlueChannel c@ColorConstructor{} = blue c
getBlueChannel c = getBlueChannel $ getColor c


sumLT255 :: Int -> Int -> Int
sumLT255 n1 n2 = min 255 (n1+n2)

sumColor :: Color -> Color -> Color
sumColor c1 c2 = ColorConstructor (sumLT255 (getRedChannel c1) (getRedChannel c2)) (sumLT255 (getGreenChannel c1) (getGreenChannel c2)) (sumLT255 (getBlueChannel c1) (getBlueChannel c2))