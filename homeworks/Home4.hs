
data Expr = Val Int
          | Plus Expr Expr
          | Minus Expr Expr
          | Mult Expr Expr
          | Div Expr Expr
          | Mod Expr Expr
           deriving (Eq,Show)

eval :: Expr -> Int
eval (Val n) = n
eval (Plus expr1 expr2) = eval expr1 + eval expr2
eval (Minus e1 e2) = eval e1 - eval e2
eval (Mult e1 e2) = eval e1 * eval e2
eval (Mod e1 e2) =  eval e1 `mod` eval e2 
eval (Div e1 e2) = eval e1 `quot` eval e2

