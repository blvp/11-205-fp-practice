

{- Нахождение максимума в списке -}
myMaximum :: [Int] -> Int
myMaximum [] = error "empty list"
myMaximum [x] = x
myMaximum (x:xs) | x > t = x 
                 | otherwise = t
                 where t = myMaximum xs        

-- | Нахождение максимум и его индекс 
-- >>> myMaxIndex [1,5,2,3,4]
-- (1,5)
myMaxIndex :: [Integer] -> (Int,Integer)
myMaxIndex l = 
   let mmax :: [Integer] -> Int -> (Int, Integer)
       mmax [x] xi = (xi, x)
       mmax (x:xs) xi | x > t = (xi, x)
                      | otherwise = (ti, t)
                      where (ti, t) = mmax xs (xi + 1)
   in mmax l 0

-- | Количество элементов, равных максимальному
-- >>> maxCount [1,5,3,10,3,10,5]
-- 2
maxCount :: [Int] -> Int
maxCount l = maxCount' l (myMaximum l) where
  maxCount' [x] t | x == t = 1
                  | otherwise = 0
  maxCount' (x:xs) t | x == t = 1 + maxCount' xs t
                     | otherwise = maxCount' xs t
-- | Количество элементов между минимальным и максимальным
-- >>> countBetween [-1,3,100,3]
-- 2
--
-- >>> countBetween [100,3,-1,3]
-- -2
--
-- >>> countBetween [-1,100]
-- 1
--
-- >>> countBetween [1]
-- 0
countBetween :: [Integer] -> Int
countBetween l = ind where
  maxIndex = fst $ myMaxIndex l
  myMinIndex :: [Integer] -> Int -> (Int, Integer)
  myMinIndex [x] xi = (xi, x)
  myMinIndex (x:xs) xi | x < t = (xi, x)
                       | otherwise = (ti, t)
                       where (ti, t) = myMinIndex xs (xi + 1)
  minIndex = fst $ myMinIndex l 0
  ind = maxIndex - minIndex 