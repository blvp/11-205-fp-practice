data BinaryTree a = Empty | Node (BinaryTree a) a (BinaryTree a)

root :: BinaryTree a -> a
root (Node _ k _) = k

height :: BinaryTree a -> Int
height Empty = -1
height (Node l _ r) = 1 + max (height l) (height r)

tmap (a -> b) -> BinaryTree a -> BinaryTree b
tmap _ Empty = Empty
tmap f (Node l k r) = Node (tmap f l) (f k) (tmap f r)

