data DList a b = Empty 
           | Cons (a DList b a)
           | Cons (b DList a b)

data ISList = DList Int String

size :: ISList -> Int
size Cons (x Empty) = 1
size Cons (_ l) = 1 + size l


dmap :: (a -> a) -> (b -> b) -> DList a b -> DList a b

dmap _ _ Empty = Empty
dmap f _ Cons(b DList) = Cons((f b) Empty)
dmap _ g Cons(s Empty) = Cons((g s) Empty)
dmap f g Cons(b Cons(s l)) = 
