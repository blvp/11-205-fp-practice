data Tree = Empty | Node Tree Int Tree


root :: Tree -> Int
root (Node _ k _) = k

height :: Tree -> Int

height Empty = -1
height (Node l _ r) = 1 + max (height l) (height r)

findElem :: Tree -> Bool
findElem x_ Empty = False
findElem x (Node left k right)
  | k == x = True
  | k >  x = findElem x left
  | k <  x = findElem x right

size :: Tree -> Int

size Empty = 0
size (Node l _ r) = 1 + size l + size r


