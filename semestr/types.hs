type Context = [(String, Binding)]

data Binding = NameBind
             | VarBind Ty


addBinding :: Context -> String -> Binding -> Context

addBinding ctx x bind = (x, bind):ctx

getBinding  :: Context -> String -> Binding

getBinding (x:xs) name | fst x == name = snd x
                       | otherwise = getBinding xs name

data Ty = TyBool
    | TyArr Ty Ty
    deriving (Eq,Show)

data Term = TmTrue
      | TmFalse
      | TmIf Term Term Term
      | TmVar String
      | TmAbs String Ty Term
      | TmApp Term Term
      deriving Show

typeOf :: Context -> Term -> Ty

typeOf ctx TmTrue = TyBool

typeOf ctx TmFalse = TyBool

typeOf ctx (TmIf p c a)
    | typeOf ctx p /= TyBool = error "predicate must be a bool"
    | otherwise = let tyC = typeOf ctx c
                      tyA = typeOf ctx a
                  in if tyC == tyA
                     then tyC
                     else error "conditional and alternative of an if-statement must be of the same type"

typeOf ctx v@(TmVar name)
    = let (VarBind ty) = getBinding ctx name
      in ty

typeOf ctx (TmAbs name tyVar body)
       = let ctx' = addBinding ctx name $ VarBind tyVar
             tyBody = typeOf ctx' body
         in TyArr tyVar tyBody
typeOf ctx (TmApp t1 t2)
    = let tyT1 = typeOf ctx t1
          tyT2 = typeOf ctx t2
      in case tyT1 of
           (TyArr tyArr1 tyArr2) -> if tyT2 == tyArr1
                                     then tyArr2
                                     else error "type mismatch in TmApp"
           otherwise -> error "TmApp must be of an arrow type"


-- (Bool->Bool) -> (Bool->Bool)
simpleLambda = TmAbs "x" (TyArr TyBool TyBool) (TmVar "x")

-- (lambda x : Bool->Bool. x) (lambda y:Bool. y) 
simpleLambda2 = TmApp (TmAbs "x" (TyArr TyBool TyBool) (TmVar "x")) (TmAbs "y" TyBool (TmVar "y"))

simpleLambda3 = TmAbs "x" TyBool TmTrue