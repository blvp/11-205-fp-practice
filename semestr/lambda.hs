data Expr = V String 
                | L String Expr
                | A Expr Expr
                deriving Show

type Context = [(String, Expr)]

show' :: Expr -> String
show' (V a) = a
show' (L x e) = "(λ " ++ x ++ ". " ++ show' e ++ ")"
show' (A e1 e2) = "(" ++ show' e1 ++ " " ++ show' e2 ++ ")"

eval :: Context -> Expr -> Expr
eval context e@(V a) = maybe e id (lookup a context)
eval context (L x e) = L x (eval context e)
eval context (A e1 e2) = apply context (eval context e1) (eval context e2)

apply :: Context -> Expr -> Expr -> Expr
apply context (L x e) e2 = eval ((x, e2):context) e
apply context e1 e2 = A e1 e2

num0 = L "f" (L "x" (V "x"))
num1 = A suc num0
num2 = A suc num1
num3 = A suc num2
num4 = A suc num3

true = L "x" (L "y" (V "x"))
false = L "x" (L "y" (V "y"))
suc = L "n" (L "f" (L "x"
         (A (V "f") 
            (A (A (V "n") (V "f"))
               (V "x")))))
add = L "m" (L "n" (L "f" (L "x" 
                             (A (A (V "m") (V "f"))
                                (A (A (V "n") (V "f"))
                                   (V "x"))))))

constF t = L "x" t
idF = L "x" (V "x")
           
testExpected = L "a" (V "a")
testActual = eval [] (A (A (L "x" (L "y" (V "x"))) (L "a" (V "a"))) (A (L "x" (A (V "x") (V "x"))) (L "x" (A (V "x") (V "x")))))

testFails = [ eval [] (A (constF idF) idF) /= L "x" (V "x")
            , eval [] (A (L "y" idF) idF)  == L "x" (V "x") ]
             
