
-- data Maybe a = Nothing
--              | Just a

myFromMaybe :: a -> Maybe a -> a
myFromMaybe def Nothing = def
myFromMaybe def (Just val) = val

myCatMaybes :: [Maybe a] -> [a]
myCatMaybes [] = []
myCatMaybes (Nothing : xs) = myCatMaybes xs
myCatMaybes ((Just x) : xs) = x : myCatMaybes xs

myCatMaybes' = map myFromJust . filter myIsJust
    where myIsJust Nothing = False
          myIsJust (Just _) = True
          myFromJust (Just x) = x 

myIsJust' = myMaybe False (\x -> True)
myIsNothing' = myMaybe True (\x -> False)
myFromMaybe' def ma = myMaybe def (\x -> x) ma -- flip myMaybe id

myMaybe :: b -> (a -> b) -> Maybe a -> b
myMaybe val f Nothing = val
myMaybe val f (Just a) = f a

data Tree a = Empty
            | Branch (Tree a) a (Tree a)
              deriving (Show)

tr1 :: Tree Int
tr1 = Branch Empty 4 (Branch (Branch Empty 3 Empty) 2 (Branch (Branch Empty 1 Empty) 5 Empty))

valueOfNodeWith2Children :: Tree a -> Maybe a
valueOfNodeWith2Children Empty = Nothing
valueOfNodeWith2Children (Branch Empty a x) = valueOfNodeWith2Children x
valueOfNodeWith2Children (Branch x a Empty) = valueOfNodeWith2Children x
valueOfNodeWith2Children (Branch x a y) = Just a

-- data Either a b = Left a
--                 | Right b

e :: Bool -> Either String Int
e False = Left "abcxc"
e True = Right 3

